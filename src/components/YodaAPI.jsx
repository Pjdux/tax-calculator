import { useState, useEffect } from "react";

export const YodaAPI = () => {
  const [formData, setFormData] = useState("");
  const [text, setText] = useState("");

  const handleChange = (e) => {
    e.preventDefault();
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };


  const handleSubmit = (e) => {
    e.preventDefault();
    const url = `https://api.funtranslations.com/translate/yoda.json?text=${formData}`;
    fetch(url).then(res => res.json()).then(data => {
        console.log(data);
        
    })
 
  };

  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <h3>{text}</h3>
        <input type="text" name="yoda" onChange={handleChange} />
        <input type="submit" value="YODIFY TEXT" />
      </form>
    </div>
  );
};
