import { useRef, useState, useEffect } from "react";
import "./components_css/TaxCalc.css";

const welcomeMessage = "Enter a dollar amount and tax percentage";
const warning = "You must enter a dollar amount and tax percentage";
const successMessage = "There is your total, search new total";

function TaxCalc() {
  const [userMessage, setUserMessage] = useState(welcomeMessage);
  const [isEditing, setEditing] = useState(false);
  const inputRef = useRef(null);

  const [result, setResult] = useState({
    saleTax: "",
    saleTotal: "",
    totalTax: null,
    totalWithTax: null,
    subtotal: null,
  });

  const tax = parseFloat(result.saleTotal) * (parseFloat(result.saleTax) / 100);

  useEffect(() => {
    if (isEditing) {
      inputRef.current.focus();
    }
  }, [isEditing]);

  const toggleEditing = () => {
    setEditing(!isEditing);
  };

  const handleChange = (e) => {
    e.preventDefault();
    setResult({ ...result, [e.target.name]: e.target.value });
  };

  const handleCalculate = (e) => {
    e.preventDefault();

    if (!result.saleTotal || !result.saleTax) {
      setUserMessage(<p>{warning}</p>);
      inputRef.current.focus();
      return;
    }

    const totalTax = tax;
    const totalWithTax = parseFloat(result.saleTotal) + totalTax;

    setResult({
      saleTax: "",
      saleTotal: "",
      totalTax,
      totalWithTax,
      subtotal: parseFloat(result.saleTotal),
    });

    setUserMessage(<p>{successMessage}</p>);
    toggleEditing();
  };

  const handleLocationClick = (taxRate) => {
    setResult((prev) => {
      const totalTax = tax;
      const totalWithTax = parseFloat(result.saleTotal) + totalTax;

      return {
        ...prev,
        saleTax: taxRate.toString(),
        saleTotal: prev.saleTotal,
        totalTax,
        totalWithTax,
        subtotal: parseFloat(result.saleTotal),
      };
    });

    setUserMessage(successMessage);
    toggleEditing();
  };

  const locations = [
    { name: "Florida", taxRate: 6.5 },
    { name: "Chicago", taxRate: 10.5 },
    { name: "Puerto Rico", taxRate: 11.5 },
    { name: "New York City", taxRate: 4.5 },
    { name: "Springfield", taxRate: 6.25 },
    { name: "Arlington", taxRate: 8.25 },
  ];

  const inputStyle = {
    color: "#ffffff",
  };

  return (
    <div className="container">
      <h2>{userMessage}</h2>
      <form onSubmit={handleCalculate}>
        <div className="inile-flex">
          <input
            ref={inputRef}
            name="saleTotal"
            type="number"
            step="any"
            placeholder="Sale Total"
            value={result.saleTotal}
            onChange={handleChange}
            className="totals-input"
          />

          <input
            name="saleTax"
            type="number"
            step="any"
            placeholder="Sale Tax"
            value={result.saleTax}
            onChange={handleChange}
            className="totals-input"
          />
        </div>

        {result.totalTax && result.totalWithTax && (
          <div className="totals-container">
            <h2>Subtotal: ${result.subtotal.toFixed(2)}</h2>
            <h2>Tax: ${result.totalTax.toFixed(2)}</h2>
            <h2>Grand Total: ${result.totalWithTax.toFixed(2)}</h2>
          </div>
        )}

        <div className="form-buttons">
          {locations.map((location) => (
            <button
              key={location.name}
              type="button"
              onClick={() => handleLocationClick(location.taxRate)}
            >
              {location.name}
            </button>
          ))}
          <button type="button" className="centered" onClick={handleCalculate}>
            Calculate
          </button>
          <input type="submit" style={{ display: "none" }} />
        </div>
      </form>
    </div>
  );
}

export default TaxCalc;
