import "./App.css";
import { ExampleBox } from "./components/ExampleBox";
import TaxCalc from "./components/TaxCalc";
import { useState } from "react";
import logo from "./assets/react.svg";
import { DogApiUI } from "./components/DogApiUI";

import { YodaAPI } from "./components/YodaAPI";
import { LandingPage } from "./components/LandingPage";
import { USStatesDropdown } from "./components/USStatesDropdown";
import {BrowserRouter as Router} from 'react-router-dom';
import NavBar from "./components/NavBar";
import AppRouter from "./client-routes/AppRouter";

function App() {
  function renderLoading() {
    return `Loading...`;
  }

  return (

 <>
      <NavBar />
      <AppRouter />
 </>

  );
}

export default App;
