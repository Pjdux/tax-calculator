import { useState } from "react";
import './components_css/USStatesDropdown.css';
const states = [
  "Alabama",
  "Alaska",
  "Arizona",
  "Arkansas",
  "California",
  "Colorado",
  "Connecticut",
  "Delaware",
  "Florida",
  "Georgia",
  "Hawaii",
  "Idaho",
  "IllinoisIndiana",
  "Iowa",
  "Kansas",
  "Kentucky",
  "Louisiana",
  "Maine",
  "Maryland",
  "Massachusetts",
  "Michigan",
  "Minnesota",
  "Mississippi",
  "Missouri",
  "MontanaNebraska",
  "Nevada",
  "New Hampshire",
  "New Jersey",
  "New Mexico",
  "New York",
  "North Carolina",
  "North Dakota",
  "Ohio",
  "Oklahoma",
  "Oregon",
  "PennsylvaniaRhode Island",
  "South Carolina",
  "South Dakota",
  "Tennessee",
  "Texas",
  "Utah",
  "Vermont",
  "Virginia",
  "Washington",
  "West Virginia",
  "Wisconsin",
  "Wyoming",
];


export const USStatesDropdown = () => {

      
  const [displayedStates, setDisplayedStates] = useState(false);
  const [state, setState] = useState('');

const handleChange = (e) => {
    e.preventDefault();
setState({...state, [e.target.name]: e.target.value});

}

const statesList = states.map((states, stateIndex) => (
    <li key={stateIndex}>
      <label>{states}</label>
      <input type="checkbox" value={state} name={`state ${state}`} onChange={handleChange} />
    </li>
  ));

  return (
    <div>
      <span>Choose A State</span>
      <ul className="states-display">
        {statesList}
      </ul>
    </div>
  );
};
