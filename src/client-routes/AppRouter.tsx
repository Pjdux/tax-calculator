import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { DogApiUI } from "../components/DogApiUI";
import { BoredAPI } from "../components/BoredAPI";
import SalesTaxCalc from "../components/TaxCalc";
import { TempConverter } from "../components/TempConverter";

const AppRouter: React.FC = () => {
  return (
    <Routes>
      <Route path="/" element={<DogApiUI />} />
      <Route path="/BoredAPI" exact element={<BoredAPI />} />
      <Route path="/SalesTaxCalc" exact element={<SalesTaxCalc />} />
      <Route path="/TempConverter" exact element={<TempConverter />} />
    </Routes>
  );
};

export default AppRouter;
