import React, { useState } from "react";

export const TempConverter = () => {
  const [degrees, setDegrees] = useState<number>(0);
  const [pickedDegree, setPickedDegree] = useState<string | null>(null);

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = parseFloat(e.target.value);
    setDegrees(isNaN(value) ? 0 : value);
  };

  const handleCelsiusConvertion = () => {
    setPickedDegree(`${degrees}°C`);
  };

  const handleFahrenheitConvertion = () => {
    setPickedDegree(`${(degrees * 9) / 5 + 32}°F`);
  };

  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <div className="inile-flex">
          <input
            name="setDegrees"
            type="number"
            step="any"
            placeholder="Enter Degrees"
            onChange={handleChange}
          />
        </div>

        <div className="form-buttons">
          <button onClick={handleCelsiusConvertion}>
            <span>&#8451;</span>
          </button>
          <button onClick={handleFahrenheitConvertion}>
            <span>&#8457;</span>
          </button>
        </div>
      </form>

      {pickedDegree && (
        <div>
          <span>{pickedDegree}</span>
        </div>
      )}
    </div>
  );
};
