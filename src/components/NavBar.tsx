// NavBar.tsx
import React from "react";
import { Link } from "react-router-dom";

const NavBar: React.FC = () => {
  return (
    <nav className="navbar">
      <ul className="nav-list">
        <li className="nav-item">
          <Link to="/" className="nav-link">
            Home
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/BoredAPI" className="nav-link">
            Bored
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/salesTaxCalc" className="nav-link">
            Tax / IVU
          </Link>
        </li>

        <li className="nav-item">
          <Link to="/TempConverter" className="nav-link">
            <span>&#8457;</span> <span>&#8451;</span>
          </Link>
        </li>
        {/* Add more links for other pages here */}
      </ul>
    </nav>
  );
};

export default NavBar;
