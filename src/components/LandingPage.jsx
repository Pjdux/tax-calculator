
export const LandingPage = () => {
const heroImage = `https://images.unsplash.com/photo-1593642702946-b2a1b1eb32c9?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80)`;

  return (
    <div className="landing-container">
      <div className="hero-img">

      </div>
      <h1>Welcome to my site</h1>
      <h2>Learn more about my product or service</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed euismod, magna vel gravida tempus, nibh diam fermentum massa</p>
    </div>
  )
}

export default LandingPage;