//import { useFetch } from '../Hooks/useFetch';
import { useState } from "react";
import { useEffect } from "react";

export function DogApiUI() {
  const [userData, setUserData] = useState("");

  useEffect(() => {

fetchDoggyData();

  },[])

  async function fetchDoggyData() {
    const url = "https://dog.ceo/api/breeds/image/random";
    
    const res = await fetch(url);
    const data = await res.json();

    console.log(data);
    setUserData(data);
    
  }



  const handleClick = () => {
    fetchDoggyData();
  };

  return (
    <div className="container">
      {userData && (
        <img

          className="doggy-style"
          src={userData.message}
          alt="random-doggy-img"
        />
      )}
      <button onClick={handleClick}>Get Random Doggie</button>
    </div>
  );
}
