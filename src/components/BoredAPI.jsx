
import { useState } from 'react';
export const BoredAPI = () => {

const [data, setData] = useState('');

const fetchData = () => {
    const url = `https://www.boredapi.com/api/activity`;
    fetch(url).then(res => res.json()).then(data => {
        console.log(data);
        setData(data);
    })
}

const handleClick = () => {
    fetchData()
}

return (
    <div className="container">
<p>Click the Button and fetch something random to do.</p>

{data && <h2>{data.activity}</h2> }

<button onClick={handleClick}>Show Me Something To Do</button>
    </div>
)

}

